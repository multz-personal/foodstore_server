const mongoose = require('mongoose')
const { model, Schema } = mongoose
const bcrypt = require('bcrypt')
const AutoIncrement = require('mongoose-sequence')(mongoose)

const HASH_ROUND = 10

let userSchema = Schema({
  full_name: {
    type: String
  },
  customer_id: {
    type: Number
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  role: {
    type: String
  },
  token: [String]
}, { timestaps: true })

userSchema.path('email').validate(function(value) {
  const EMAIL_RE = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
  return EMAIL_RE.test(value)
}, attr => `${attr.value} bukan email yang valid!`)

userSchema.path('email').validate(async function(value) {
  try {
    const count = await this.model('User').count({email: value})
    return !count
  } catch (err) {
    throw err
  }
}, attr => `${attr.value} sudah terdaftar`)

userSchema.pre('save', function(next) {
  this.password = bcrypt.hashSync(this.password, HASH_ROUND)
  next()
})

userSchema.plugin(AutoIncrement, {inc_field: 'customer_id'})

module.exports = model('User', userSchema)